# User Map Location

A [Flarum](http://flarum.org) extension. Adds an optional Location Attribute and a map to users settings, as well as a global map that displays all users that have opted in.

This fork is based on [the plugin from justoverclock](https://github.com/justoverclockl/users-map-location), with some improvements, and notably a global map where it is possible to see the location of all forum users that have filled this information.

No precise location is disclosed, only city or municipality, and only for users who want to indicate this information.

## Installation

```
composer require youen/users-map
```
