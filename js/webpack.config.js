const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = require('flarum-webpack-config')();

module.exports.plugins.push(new MiniCssExtractPlugin({filename: '[name].less'}));

module.exports.resolve.extensions.push('.less');

module.exports.module.rules.push({
	test: /\.less$/i,
	use: [
		MiniCssExtractPlugin.loader,
		"css-loader",
		//"less-loader", // We don't compile less at this stage, flarum already compiles less at runtime. Compiling twice can cause issues.
	],
});

module.exports.module.rules.push({
	test: /\.css$/i,
	use: [
		MiniCssExtractPlugin.loader,
		"css-loader"
	],
});