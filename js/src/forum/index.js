import app from 'flarum/forum/app';
import { extend } from 'flarum/common/extend';
import SettingsPage from 'flarum/forum/components/SettingsPage';
import User from 'flarum/common/models/User';
import Model from 'flarum/common/Model';
import AddLocationComponent from './components/AddLocationComponent';
import UserLocation from './models/UserLocation';
import UserCard from 'flarum/forum/components/UserCard';
import HeaderPrimary from 'flarum/forum/components/HeaderPrimary';
import Link from 'flarum/common/components/Link';
import Leaflet from 'leaflet';
import 'leaflet.markercluster';
import GlobalMapPage from './components/GlobalMapPage';

import "./../../../less/forum.less";
import "./../../node_modules/leaflet/dist/leaflet.css";
import "./../../node_modules/leaflet.markercluster/dist/MarkerCluster.css";
import "./../../node_modules/leaflet.markercluster/dist/MarkerCluster.Default.css";

app.initializers.add('youen/users-map', (app) => {
  User.prototype.location_country = Model.attribute('location_country');
  User.prototype.location_countrycode = Model.attribute('location_countrycode');
  User.prototype.location_postcode = Model.attribute('location_postcode');
  User.prototype.location_city = Model.attribute('location_city');
  User.prototype.location_latitude = Model.attribute('location_latitude');
  User.prototype.location_longitude = Model.attribute('location_longitude');
  
  app.store.models.userlocations = UserLocation;
  
  app.routes['youen.global-map'] = { path: '/global-map', component: GlobalMapPage };
  
  extend(UserCard.prototype, 'infoItems', function (items) {
    const user = this.attrs.user;
    
    if(user.location_latitude()) {
      items.add('mapLocation', <div className="location-map location-map-user-profile"/>, -100);
    }
  });

  extend(UserCard.prototype, 'oncreate', function (originalResult, vnode) {

    let script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = app.forum.attribute('baseUrl') + '/assets/extensions/youen-users-map/leaflet.edgebuffer.js'

    document.head.appendChild(script);
    
    const user = this.attrs.user;
    
    let location = user.location_latitude()
      ? {
        lat: user.location_latitude(),
        lon: user.location_longitude()
      }
      : null;

    if (!location) return;
    
    let mapElement = vnode.dom.getElementsByClassName('location-map')[0];

    const publicToken = app.forum.attribute('youen-users-map.mapbox-api-key');
    const markerIconPath = app.forum.attribute('baseUrl') + '/assets/extensions/youen-users-map/marker-icon.png';

    let markerIcon = L.icon({
      iconUrl: markerIconPath,
      iconSize: [25, 41], // size of the icon
      iconAnchor: [13, 40]
    });

    let map = L.map(mapElement).setView([location.lat, location.lon], 13);
    let marker = L.marker([location.lat, location.lon], { icon: markerIcon }).addTo(map);
    let layerUserCard = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
      attribution:
        'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery &copy; <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      edgeBufferTiles: 1,
      id: 'mapbox/streets-v11',
      tileSize: 512,
      zoomOffset: -1,
      accessToken: publicToken,
    }).addTo(map);
    /*setTimeout(() => {
      map.invalidateSize();
    },100);*/
    function onMapResized() {
      map.invalidateSize();
    }
    onMapResized();

    new ResizeObserver(onMapResized).observe(mapElement);
  });

  extend(SettingsPage.prototype, 'settingsItems', function (items) {
    items.add('location', <AddLocationComponent />);
  });
});

extend(HeaderPrimary.prototype, 'onupdate', function() {
	let homeLink = document.getElementById('home-link');
	if(homeLink) {
		let globalMapLink = document.getElementById('global-map-link');
		if(!globalMapLink) {
			globalMapLink = document.createElement('span');
			globalMapLink.setAttribute('id', 'global-map-link');
			homeLink.parentElement?.append(globalMapLink);
			
			m.render(globalMapLink, <Link href="/global-map" id="map-link" class="Button Button--primary">
				{app.translator.trans('youen-users-map.forum.global-map.linkToMap')}
			</Link>);
		}
	}
});

export {
  AddLocationComponent
}
