import type Mithril from 'mithril';
import app from 'flarum/forum/app';
import Page from 'flarum/common/components/Page';

function translate(id: string): string {
	return app.translator.trans(id) as string;
}

const escapeHtml = (unsafe) => {
    return unsafe.replaceAll('&', '&amp;').replaceAll('<', '&lt;').replaceAll('>', '&gt;').replaceAll('"', '&quot;').replaceAll("'", '&#039;');
}

export default class GlobalMapPage extends Page {
	oninit(vnode: Mithril.Vnode) {
		super.oninit(vnode);

		app.setTitle(translate('youen-users-map.forum.global-map.title'));
		
		this.map = null;
	}

	view() {
		return <div class="GlobalMapPage">
			<div className="container">
				<h2>{translate('youen-users-map.forum.global-map.title2')}</h2>
				<p>{translate('youen-users-map.forum.global-map.description')}</p>
				<div className="global-map"></div>
			</div>
		</div>;
	}
		
	oncreate(vnode) {
		this.onupdate(vnode);
	}
		
	onupdate(vnode) {
		let dom = vnode.dom;
		let mapElements = dom.getElementsByClassName('global-map');

		if(mapElements.length > 0) {
			if(!this.map) {
				let mapElement = mapElements[0];

				const publicToken = app.forum.attribute('youen-users-map.mapbox-api-key');
				const markerIconPath = app.forum.attribute('baseUrl') + '/assets/extensions/youen-users-map/marker-icon.png';

				this.userMarkerIcon = L.icon({
					iconUrl: markerIconPath,
					iconSize: [25, 41], // size of the icon
					iconAnchor: [13, 40]
				});

				this.map = L.map(mapElement);
				let layer = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
					attribution:
					'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery &copy; <a href="https://www.mapbox.com/">Mapbox</a>',
					maxZoom: 18,
					edgeBufferTiles: 1,
					id: 'mapbox/streets-v11',
					tileSize: 512,
					zoomOffset: -1,
					accessToken: publicToken,
				}).addTo(this.map);
				
				this.map.setView([47, 2], 6);
				
				this.userMarkers = L.markerClusterGroup({ maxClusterRadius: 40 });
				this.map.addLayer(this.userMarkers);
				
				let usersExpirationDelay = 10 * 60; // in seconds
				if(app.store.lastFetchedUserLocations && Date.now() - app.store.lastFetchedUserLocations < usersExpirationDelay * 1000)
				{
					// reuse data from the store
					this.displayUsers(app.store.all('users'));
				} else {
					// begin fetching users
					this.fetchUsers(0);
				}
			}
		} else {
			this.map = null;
		}
	}

	fetchUsers(offset) {
		let maxLocationsPerRequest = 50;
		app.store.find('userlocations', { page: { limit: maxLocationsPerRequest, offset: offset } }).then((response) => {
			this.displayUsers(response);
			
			if(response.length == maxLocationsPerRequest) {
				// we need to fetch more users
				this.fetchUsers(offset + response.length);
			}
			else {
				// finished fetching all users
				app.store.lastFetchedUserLocations = Date.now();
			}
		});
	}

	displayUsers(users) {
		for(let item of users) {
			let user = item.data.attributes;
			if(user.location_latitude) {
				let marker = L.marker([parseFloat(user.location_latitude), parseFloat(user.location_longitude)], { icon: this.userMarkerIcon });
				marker.bindPopup('<a href="/u/'+escapeHtml(user.username)+'">'+escapeHtml(user.displayName)+'</a>');
				this.userMarkers.addLayer(marker);
			}
		}
	}
}
