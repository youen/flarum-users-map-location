import Model from 'flarum/common/Model';

export default class UserLocation extends Model {
  username = Model.attribute('username');
  displayName = Model.attribute('displayName');
  location_latitude = Model.attribute('location_latitude');
  location_longitude = Model.attribute('location_longitude');
}
