import app from 'flarum/admin/app';
import { extend } from 'flarum/common/extend';
import UserListPage from 'flarum/admin/components/UserListPage';

app.initializers.add('youen/users-map', () => {
  app.extensionData.for('youen-users-map').registerSetting({
    setting: 'youen-users-map.mapbox-api-key',
    name: 'youen-users-map.mapbox-api-key',
    type: 'text',
    label: app.translator.trans('youen-users-map.admin.mapbox-api-key'),
    help: app.translator.trans('youen-users-map.admin.mapbox-api-key-help'),
  });
  extend(UserListPage.prototype, 'columns', function (items) {
    items.add(
      'location',
      {
        name: app.translator.trans('youen-users-map.admin.adminLocationField'),
        content: (user) => {

          return <div>{user.data.attributes.location}</div>;
        },
      },
      -50
    );
  });
});
