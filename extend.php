<?php

/*
 * This file is part of youen/users-map.
 *
 * Copyright (c) 2022 Marco Colia.
 * Copyright (c) 2024 Youen Toupin.
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Youen\UsersMap;


use Youen\UsersMap\Listeners\SaveLocationToDatabase;
use Youen\UsersMap\Listeners\AddLocationAttribute;
use Flarum\Api\Serializer\UserSerializer;
use Flarum\Extend;
use Flarum\User\Event\Saving;
use Flarum\Api\Event\Serializing;
use Flarum\User\Filter\UserFilterer;
use Flarum\Filter\FilterState;
use Flarum\Query\QueryCriteria;

class UserLocationFilterMutator
{
    public function __invoke(FilterState $filterState, QueryCriteria $queryCriteria)
    {
        if($queryCriteria->mustHaveLocation)
        {
            $filterState->getQuery()->where('location_latitude', '!=', 'null');
        }
    }
}

return [
    (new Extend\Frontend('forum'))
        ->js(__DIR__.'/js/dist/forum.js')
        ->css(__DIR__.'/js/dist/forum.less'),
    
    (new Extend\Frontend('admin'))
        ->js(__DIR__.'/js/dist/admin.js')
        ->css(__DIR__.'/less/admin.less'),
    
    new Extend\Locales(__DIR__.'/locale'),
    
    (new Extend\Event())
        ->listen(Saving::class, SaveLocationToDatabase::class),

    (new Extend\ApiSerializer(UserSerializer::class))
        ->attributes(AddLocationAttribute::class),

    (new Extend\Settings)->serializeToForum('youen-users-map.mapbox-api-key', 'youen-users-map.mapbox-api-key'),
	
	(new Extend\Frontend('forum'))
        ->route('/global-map', 'youen-users-map.global-map'),
    
    (new Extend\Filter(UserFilterer::class))
        ->addFilterMutator(UserLocationFilterMutator::class),
    
    (new Extend\Routes('api'))
        ->get('/userlocations', 'userlocations.index', Api\Controller\ListUserLocationsController::class)
];
