<?php

namespace Youen\UsersMap\Api\Controller;

use Flarum\Api\Controller\AbstractListController;
use Flarum\Http\RequestUtil;
use Flarum\Http\UrlGenerator;
use Psr\Http\Message\ServerRequestInterface;
use Tobscure\JsonApi\Document;
use Youen\UsersMap\Api\Serializer\UserLocationSerializer;
use Flarum\User\User;
use Flarum\Query\QueryCriteria;
use Flarum\User\Filter\UserFilterer;
use Flarum\User\Search\UserSearcher;

use Illuminate\Database\Eloquent\Builder;

class ListUserLocationsController extends AbstractListController
{
	/**
	 * {@inheritdoc}
	 */
	public $serializer = UserLocationSerializer::class;
	
	/**
	 * @var UserFilterer
	 */
	protected $filterer;

	/**
	 * @var UserSearcher
	 */
	protected $searcher;

	/**
	 * @var UrlGenerator
	 */
	protected $url;

	/**
	 * @param UserLocationFilterer $filterer
	 * @param UserSearcher $searcher
	 * @param UrlGenerator $url
	 */
	public function __construct(UserFilterer $filterer, UserSearcher $searcher, UrlGenerator $url)
	{
		$this->filterer = $filterer;
		$this->searcher = $searcher;
		$this->url = $url;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function data(ServerRequestInterface $request, Document $document)
	{
		// See https://docs.flarum.org/extend/api.html#api-endpoints for more information.

		$actor = RequestUtil::getActor($request);
		
		// We do not limit discovery of users in this method, because we only reveal those who have accepted to appear on the global map (they have defined a location in their profile)
		// Also, this method only returns attributes related to the map (like username and location)
		//$actor->assertCan('searchUsers');

		if (! $actor->hasPermission('user.viewLastSeenAt')) {
			// If a user cannot see everyone's last online date, we prevent them from sorting by it
			// Otherwise this sort field would defeat the privacy setting discloseOnline
			// We use remove instead of add so that extensions can still completely disable the sort using the extender
			$this->removeSortField('lastSeenAt');
		}

		$filters = $this->extractFilter($request);
		$sort = $this->extractSort($request);
		$sortIsDefault = $this->sortIsDefault($request);

		$limit = $this->extractLimit($request);
		$offset = $this->extractOffset($request);
		$include = $this->extractInclude($request);

		$criteria = new QueryCriteria($actor, $filters, $sort, $sortIsDefault);
		$criteria->mustHaveLocation = true;
		
		// As of now, search is disabled, because in current implementation it could disclose users who do not want to appear on the map (no location)
		/*if (array_key_exists('q', $filters)) {
			$results = $this->searcher->search($criteria, $limit, $offset);
		} else {*/
			$results = $this->filterer->filter($criteria, $limit, $offset);
		//}

		$document->addPaginationLinks(
			$this->url->to('api')->route('userlocations.index'),
			$request->getQueryParams(),
			$offset,
			$limit,
			$results->areMoreResults() ? null : 0
		);
		
		$results = $results->getResults();

		$this->loadRelations($results, $include, $request);

		return $results;
	}
}
