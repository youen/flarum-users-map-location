<?php

namespace Youen\UsersMap\Api\Serializer;

use Flarum\Api\Serializer\AbstractSerializer;
use Flarum\User\User;
use InvalidArgumentException;

class UserLocationSerializer extends AbstractSerializer
{
	/**
	 * {@inheritdoc}
	 */
	protected $type = 'users';

	/**
	 * {@inheritdoc}
	 *
	 * @param User $model
	 * @throws InvalidArgumentException
	 */
	protected function getDefaultAttributes($model)
	{
		if (! ($model instanceof User)) {
			throw new InvalidArgumentException(
				get_class($this).' can only serialize instances of '.User::class
			);
		}

		// See https://docs.flarum.org/extend/api.html#serializers for more information.

		return [
			'username' => $model->username,
			'displayName' => $model->display_name,
			'location_longitude' => $model->location_longitude,
			'location_latitude' => $model->location_latitude,
		];
	}
}
